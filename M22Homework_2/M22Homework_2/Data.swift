//
//  Data.swift
//  M22Homework_2
//
//  Created by Ekaterina Burdeleva on 05.10.2023.
//

import Foundation

//получение списка фильмов
func getCinemas(search_query: String, completion: @escaping (CinemaListModel?, Error?) -> ()) {
    
    let string = "https://kinopoiskapiunofficial.tech/api/v2.1/films/search-by-keyword?keyword=\(search_query)"
    guard let urlString = string.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
        let url1 = URL(string: urlString) else {
            fatalError()
    }
    var request = URLRequest(url: url1)
    request.httpMethod = "GET"
    request.allHTTPHeaderFields = ["Content-Type" : "application/json",
                                   "X-API-KEY": "f3bc1207-7453-44f5-b4f2-62a8348c44c6",]
    request.httpBody = nil
    
    let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error)  in
        if let error = error {
            print("Error", error)
            completion(nil, error)
        }
        if let data = data {
            do {
                let cinemas = try JSONDecoder().decode(CinemaListModel.self, from: data)
                completion(cinemas, nil)
            }
            catch let jsonError {
                print("Decoding Error", jsonError)
                completion(nil, jsonError)
            }
        }
    })
    task.resume()
}


//получение детальной информации о фильме
func getDetails(filmId: Int, completion: @escaping (DetailCinema?, Error?) -> ()) {
    
    let url = URL(string: "https://kinopoiskapiunofficial.tech/api/v2.2/films/\(filmId)")!
    var request = URLRequest(url: url)
    request.httpMethod = "GET"
    request.allHTTPHeaderFields = ["Content-Type" : "application/json",
                                   "X-API-KEY": "f3bc1207-7453-44f5-b4f2-62a8348c44c6",]
    request.httpBody = nil
    
    let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error)  in
        if let error = error {
            print("Error", error)
            completion(nil, error)
        }
        if let data = data {
            do {
                let cinema = try JSONDecoder().decode(DetailCinema.self, from: data)
                completion(cinema, nil)
            }
            catch let jsonError {
                print("Decoding Error", jsonError)
                completion(nil, jsonError)
            }
        }
    })
    task.resume()
}

