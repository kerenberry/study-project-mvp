//
//  Box.swift
//  M22Homework_2
//
//  Created by Ekaterina Burdeleva on 05.10.2023.
//

import Foundation

class Box<T> {
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    func bind(_ listener: Listener?) {
        self.listener = listener
    }
    
    func bindAndFire(_ listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
    
    var value: T {
        didSet{
            print("before call listener")
            listener?(value)
            print("after call listener")
        }
    }
    
    init(_ v: T) {
        value = v
    }
}


