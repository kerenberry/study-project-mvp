//
//  CustomCell.swift
//  M22Homework_2
//
//  Created by Ekaterina Burdeleva on 06.10.2023.
//

import UIKit

class CustomCell: UITableViewCell {
    
    lazy var cinemaImage: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = UIColor.gray
        return imageView
    }()
    
    lazy var cinemaTitle: UILabel = {
        let label = UILabel()
        label.font = Constants.Fonts.ui16Semi
        return label
    }()
    lazy var cinemaDescription: UILabel = {
        let label = UILabel()
        label.font = Constants.Fonts.ui14Regular
        label.numberOfLines = 3
        label.sizeToFit()
        return label
    }()
    
    private lazy var vStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 8
        stackView.addArrangedSubview(cinemaTitle)
        stackView.addArrangedSubview(cinemaDescription)
        return stackView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .top
        stackView.spacing = 10
        stackView.addArrangedSubview(cinemaImage)
        stackView.addArrangedSubview(vStackView)
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(stackView)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setupConstraints() {
        cinemaImage.snp.makeConstraints { make in
            make.height.equalTo(50)
            make.width.equalTo(50)
        }
        stackView.snp.makeConstraints { make in
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.top.equalTo(16)
            make.center.equalToSuperview()
        }
    }
}
