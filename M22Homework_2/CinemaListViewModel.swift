//
//  CinemaListViewModel.swift
//  M22Homework_2
//
//  Created by Ekaterina Burdeleva on 05.10.2023.
//

import Foundation
import UIKit

protocol CinemaListViewModelProtocol {
    
    var itemSignal: Box<CinemaListModel?> { get }
//    var openCinemaSignal: Box<Cinema?> { get }
    
    func updateDataSource()
    
    func numberOfRawInSection(_ indexPath: IndexPath) -> Int
    
    func didSelectedCinemaAt(_ indexPath: IndexPath)
    
    func searchButtonClicked(_ searchQuery: String)
}

class CinemaListViewModel: CinemaListViewModelProtocol {

    let model: CinemaListModel? = nil
    var itemSignal: Box<CinemaListModel?> = Box(nil)
    
//    var openCinemaSignal: Box<Cinema?> = Box(nil)
    
    
    func numberOfRawInSection(_ indexPath: IndexPath) -> Int {
        return model?.films.count ?? 0
    }
    
    func updateDataSource() {
        print("WTF")
    }
    
    func didSelectedCinemaAt(_ indexPath: IndexPath) {
        print("WTF")
    }
    
    func searchButtonClicked(_ searchQuery: String) {
        let urlyfiedSearchQuery = searchQuery.replacingOccurrences(of: " ", with: "%20")
        getCinemas(search_query: urlyfiedSearchQuery) { [weak self] (searchResponse, error) in
            if let self = self {
                print("before")
                self.itemSignal.value = searchResponse! //тут как-то по другому надо раскрыть опшинал
                print("after")
        }
//                self.view?.reloadTable()
//            }
//        }
    }
    
    
}

//extension CinemaListViewModel: NSFetchedResultsControllerDelegate {
//    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>){
//
//    }
//    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>){
//
//    }
//    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any at indexPath: IndexPath?, for type NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
//        updateDataSource()
//    }
}
