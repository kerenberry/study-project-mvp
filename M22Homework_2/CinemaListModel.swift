//
//  CinemaListModel.swift
//  M22Homework_2
//
//  Created by Ekaterina Burdeleva on 05.10.2023.
//

import UIKit
import Foundation
import CoreData

struct CinemaListModel: Decodable {
    var keyword: String
    var pagesCount: Int
    var searchFilmsCountResult: Int
    var films: [Cinema]
}


struct Cinema: Decodable {
    var filmId: Int
    var nameRu: String?
    var nameEn: String?
    var posterUrlPreview: String?
}
    
//class CinemaListModel {
//    private let persistentContainer = NSPersistentContainer(name: "Model")
//
//    lazy var fetchedResultsController: NSFetchedResultsController<Cinema> = {
//        let fetchRequest = Cinema.fetchRequest()
//        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
//        fetchRequest.sortDescriptors = [sortDescriptor]
//        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.persistentContainer.viewContext,sectionNameKeyPath: nil, cacheName: nil)
//        return fetchedResultsController
//    }()
//
//    init() {
//        persistentContainer.loadPersistentStores { (persistentStoreDescription, error) in
//            if let error = error {
//                print("Unable to Load Persistent Store")
//                print("\(error), \(error.localizedDescription)")
//            } else {
//                do {
//                    try self.fetchedResultsController.performFetch()
//                } catch {
//                    print(error)
//                }
//            }
//        }
//    }
//
//    func setDelegate(_ delegate : NSFetchedResultsControllerDelegate?) {
//        fetchedResultsController.delegate = delegate
//    }
//}
    
