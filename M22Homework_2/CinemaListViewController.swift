//
//  CinemaListViewController.swift
//  M22Homework_2
//
//  Created by Ekaterina Burdeleva on 05.10.2023.
//

import UIKit
import SnapKit


class CinemaListViewController: UIViewController, UISearchBarDelegate {
    
    private let viewModel: CinemaListViewModelProtocol = CinemaListViewModel()
    var cinemaList: CinemaListModel?
    private var cinemas = [CinemaListModel]()
    
    let cellIdentifier = "cellWithImage"
    var searchQuery: String = ""

    private lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.layer.cornerRadius = 10
        searchBar.layer.borderWidth = 1
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = .clear
        searchBar.placeholder = "Type something"
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        return searchBar
    }()
    
    private lazy var searchButton: UIButton = {
        let searchButton = UIButton()
        searchButton.setTitle("Search", for:.normal)
        searchButton.layer.borderWidth = 1
        searchButton.backgroundColor = UIColor.gray
        searchButton.layer.cornerRadius = 10
        searchButton.translatesAutoresizingMaskIntoConstraints = false
        return searchButton
    }()

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: CGRect.zero, style: .grouped)
        tableView.backgroundColor = .clear
        tableView.allowsSelection = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    private lazy var hStackView: UIStackView = {
        let hStackView = UIStackView()
        hStackView.axis = .horizontal
        hStackView.spacing = 10
        hStackView.addArrangedSubview(searchBar)
        hStackView.addArrangedSubview(searchButton)
        hStackView.translatesAutoresizingMaskIntoConstraints = false
        return hStackView
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.systemBackground
        viewModel.itemSignal.bind { [weak self] item in
            self?.cinemaList = item
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        viewModel.updateDataSource()
        
        tableView.register(CustomCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = true
        searchBar.delegate = self
        searchButton.addTarget(self, action: #selector(searchButtonClicked), for: UIControl.Event.touchUpInside)
        setupViews()
        setupConstraints()

    }
    
    @objc func searchButtonClicked(sender:UIButton) {
        viewModel.searchButtonClicked(searchQuery)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchQuery = searchText
    }
    
    func setupViews() {
        view.addSubview(hStackView)
        view.addSubview(tableView)
    }

    func setupConstraints() {
        // Констрейнты со SnapKit
        
        searchButton.snp.makeConstraints { make in
            make.width.equalTo(100)
        }
        hStackView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(50)
            make.left.right.equalToSuperview().inset(20)
        }
        tableView.snp.makeConstraints { make in
            make.top.equalTo(hStackView).inset(50)
            make.left.right.equalToSuperview().inset(20)
            make.bottom.equalToSuperview().inset(20)
        }
    }

}

extension CinemaListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cinemaList?.films.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CustomCell
        let cinema = cinemaList?.films[indexPath.row]
        
        if let nameRu = cinema?.nameRu {
            cell?.cinemaTitle.text = nameRu
        }
        else {
            cell?.cinemaTitle.text = cinema?.nameEn
        }
        
        if let url = cinema?.posterUrlPreview {
            if let urls = URL(string: url) {
                DispatchQueue.global().async {
                    if let data = try? Data(contentsOf: urls),
                       let image = UIImage(data: data)
                    {
                        DispatchQueue.main.async {
                            cell?.cinemaImage.image = image
                        }
                    } else {
                        cell?.cinemaImage.image = Constants.Images.avatar
                    }
                }
            } else {
                cell?.cinemaImage.image = Constants.Images.avatar
            }
        }
        
        return cell ?? UITableViewCell()
    }
}

//MARK: - UITableViewDelegate
extension CinemaListViewController:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let dataObject = viewModel.itemSignal.value?.films[indexPath.row] else {
            return
        }
//        viewModel.didSelectedCinema(at: indexPath)
    }
    
}



//MARK: — Presenter delegate

//extension CinemaListViewController: CinemaListViewProtocol {
//    func reloadTable() {
//        DispatchQueue.main.async {
//            self.tableView.reloadData()
//        }
//    }
//
//    func openDetails(_ id: Int) {
//        let newViewController = CinemaDetailsViewController()
//        newViewController.id = id
//        self.navigationController?.pushViewController(newViewController, animated: true)
//    }
//
//}

