//
//  Constants.swift
//  M22Homework_2
//
//  Created by Ekaterina Burdeleva on 05.10.2023.
//

import UIKit

enum Constants {
    enum Fonts {
            static var ui16Semi: UIFont? {
                UIFont(name: "Inter-SemiBold", size: 16)
            }
            static var ui14Regular: UIFont? {
                UIFont(name: "Inter-Regular", size: 14)
            }
        }
    enum Images {
        static let avatar = UIImage(named: "kinopoisk_avatar")
    }
    
    enum Texts {
        static let description = "Описание фильма отсутствует"
    }
}
