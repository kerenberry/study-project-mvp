//
//  CinemaDetailsModel.swift
//  M22Homework_2
//
//  Created by Ekaterina Burdeleva on 05.10.2023.
//

import UIKit

struct DetailCinema: Decodable {
    var nameRu: String?
    var nameEn: String?
    var nameOriginal: String?
    var description: String?
    var posterUrl: String?
}
