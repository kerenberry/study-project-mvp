//
//  CinemaDetailsPresenter.swift
//  M22Homework
//
//  Created by Ekaterina Burdeleva on 20.07.2023.
//

import Foundation


protocol CinemaDetailsViewProtocol: AnyObject {
    //оставила блок на случай, если потребуется
}

protocol CinemaDetailsPresenterProtocol: AnyObject {
    
    //устанавливаем View для Presenter
    func setView(_ view: CinemaDetailsViewProtocol)
    
    //запускается при появлении экрана
    func doOnStart(_ id: Int, completion: @escaping (DetailCinema?) -> Void)

}


final class CinemaDetailsPresenter: CinemaDetailsPresenterProtocol {

    private weak var view: CinemaDetailsViewProtocol?
    var cinema: DetailCinema?
    
    
    func setView(_ view: CinemaDetailsViewProtocol) {
        self.view = view
    }
    
    func doOnStart(_ id: Int, completion: @escaping (DetailCinema?) -> Void) {
        get_details(filmId: id) { [weak self] (getResponse, error) in
            if let self = self {
                if let cinema = getResponse{
                    self.cinema = cinema
                    completion(self.cinema)
                }
            }
        }
    }
}
