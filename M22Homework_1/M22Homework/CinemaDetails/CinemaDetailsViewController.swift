//
//  CinemaDetailsViewController.swift
//  M22Homework
//
//  Created by Ekaterina Burdeleva on 20.07.2023.
//

import Foundation
import UIKit

class CinemaDetailsViewController: UIViewController, CinemaDetailsViewProtocol {
    
    var id: Int = 0
    private let detailsPresenter: CinemaDetailsPresenterProtocol = CinemaDetailsPresenter()
    let avatar = UIImage(systemName: "square.fill")

    lazy var cinemaTitle: UILabel = {
        let label = UILabel()
        label.font = Constants.Fonts.ui16Semi
        return label
    }()

    lazy var cinemaImage: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = UIColor.gray
        return imageView
    }()
    
    lazy var cinemaDescription: UILabel = {
        let label = UILabel()
        label.font = Constants.Fonts.ui14Regular
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }()
    
    private lazy var hStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .top
        stackView.spacing = 10
        stackView.addArrangedSubview(cinemaImage)
        stackView.addArrangedSubview(cinemaDescription)
        return stackView
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailsPresenter.setView(self)
        detailsPresenter.doOnStart(id) {cinemaDetails in
            DispatchQueue.main.async {
                if let nameRu = cinemaDetails?.nameRu {
                    self.cinemaTitle.text = nameRu
                }
                else if let nameEn = cinemaDetails?.nameEn {
                    self.cinemaTitle.text = nameEn
                }
                else {
                    self.cinemaTitle.text = cinemaDetails?.nameOriginal
                }
                
                if let description = cinemaDetails?.description {
                    self.cinemaDescription.text = description
                }
                else {
                    self.cinemaDescription.text = "Описание фильма отсутствует"
                }
                
            }
                if let url = cinemaDetails?.posterUrl {
                    if let urls = URL(string: url) {
                        if let data = try? Data(contentsOf: urls), let image = UIImage(data: data) {
                            DispatchQueue.main.async {
                                self.cinemaImage.image = image
                            }
                        }
                        else {
                            self.cinemaImage.image = Constants.Images.avatar
                        }
                    }
                }
                
                else {
                    self.cinemaImage.image = Constants.Images.avatar
                }
            }
        view.backgroundColor = UIColor.systemBackground
        setupViews()
        setupConstraints()
        cinemaTitle.translatesAutoresizingMaskIntoConstraints = false
        hStackView.translatesAutoresizingMaskIntoConstraints = false
        cinemaImage.translatesAutoresizingMaskIntoConstraints = false
    }


    func setupViews() {
        view.addSubview(cinemaTitle)
        view.addSubview(hStackView)
    }
    
    func setupConstraints() {
        cinemaTitle.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(100)
        }
        cinemaImage.snp.makeConstraints { make in
            make.width.equalTo(100)
            make.height.equalTo(150)
        }

        hStackView.snp.makeConstraints { make in
            make.top.equalTo(cinemaTitle).inset(50)
            make.bottom.equalToSuperview().inset(30)
            make.left.right.equalToSuperview().inset(10)
        }
    }
}

