//
//  Constants.swift
//  HomeWork_22
//
//  Created by Ekaterina Burdeleva on 10.11.2022.
//

import UIKit

enum Constants {
    enum Fonts {
            static var ui16Semi: UIFont? {
                UIFont(name: "Inter-SemiBold", size: 16)
            }
            static var ui14Regular: UIFont? {
                UIFont(name: "Inter-Regular", size: 14)
            }
        }
    enum Images {
        static let avatar = UIImage(systemName: "square.fill")
    }
}
