//
//  Cinema.swift
//  HomeWork_22
//
//  Created by Ekaterina Burdeleva on 10.01.2023.
//

import UIKit

struct SearchResponse: Decodable {
    var keyword: String
    var pagesCount: Int
    var searchFilmsCountResult: Int
    var films: [Cinema]
}


struct Cinema: Decodable {
    var filmId: Int
    var nameRu: String?
    var nameEn: String?
    var posterUrlPreview: String?
}

struct DetailCinema: Decodable {
    var nameRu: String?
    var nameEn: String?
    var nameOriginal: String?
    var description: String?
    var posterUrl: String?
}
