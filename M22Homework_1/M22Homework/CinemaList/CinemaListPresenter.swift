//
//  CinemaListPresenter.swift
//  M22Homework
//
//  Created by Ekaterina Burdeleva on 20.07.2023.
//

import Foundation


protocol CinemaListViewProtocol: AnyObject {
    
    func reloadTable()
    
    func openDetails(_ filmId: Int)
}

protocol CinemaListPresenterProtocol: AnyObject {
    
    //устанавливаем View для Presenter
    func setView(_ view: CinemaListViewProtocol)
    
    //кол-во секций в таблице
    func numberOfSections() -> Int
    
    //кол-во строк в определенной секции
    func numberOfRowInSection(_ section: Int) -> Int

    // данные для определенной ячейки
    func cinemaForRow(at indexPath: IndexPath) -> Cinema

    //вызывается при нажатии на кнопку ействия
    func searchButtonClicked(_ searchQuery: String)
    
    //вызывается при нажатии на ячейку
    func didSelectCinema(at indexPath: IndexPath)
}


final class CinemaListPresenter: CinemaListPresenterProtocol {

        
    private weak var view: CinemaListViewProtocol?
    var searchResponse: SearchResponse?
    var cinemas = [Cinema]()
    
    
    func setView(_ view: CinemaListViewProtocol) {
        self.view = view
    }
    
    func numberOfSections() -> Int {
        return 1
    }

    func numberOfRowInSection(_ section: Int) -> Int {
        return searchResponse?.films.count ?? 0
    }
    

    func cinemaForRow(at indexPath: IndexPath) -> Cinema {
        let cinema = (searchResponse?.films[indexPath.row])!
        return cinema
    }
    
    func searchButtonClicked(_ searchQuery: String) {
        let urlyfiedSearchQuery = searchQuery.replacingOccurrences(of: " ", with: "%20")
        get_cinemas(search_query: urlyfiedSearchQuery) { [weak self] (searchResponse, error) in
            if let self = self {
                self.searchResponse = searchResponse
                self.view?.reloadTable()
            }
        }
    }
    
    func didSelectCinema(at indexPath: IndexPath) {
        let id = (searchResponse?.films[indexPath.row].filmId)!
        print(id)
        view?.openDetails(id)
    }
}
